terraform {
  backend "s3" {
    bucket = "s3-bucket-terraform-backend-state"
    key    = "dev/terraform_state"
    region = "eu-central-1"
    dynamodb_table = "terraform_backend_lock"
  }
}
