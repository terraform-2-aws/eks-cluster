module "vpc" {
  source      = "./vpc"
  environment = var.environment


}

module "iam" {
  source = "./iam"
}

module "eks" {
  source       = "./eks"
  cluster_name = var.cluster_name
  environment  = var.environment
  # eks_node_group_instance_types = var.eks_node_group_instance_types
  private_subnets   = module.vpc.aws_subnets_private
  public_subnets    = module.vpc.aws_subnets_public
  fargate_namespace = var.fargate_namespace
}


module "backend" {
  source = "./backend"

}
