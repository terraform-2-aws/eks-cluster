variable "environment" {
  description = "Environment name"
  default     = "k8s"
}

variable "cluster_name" {
  description = "Name of cluster"
  default     = "Kubernetes-cluster"
}
