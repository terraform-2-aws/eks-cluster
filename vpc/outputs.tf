output "aws_subnets_public" {
  value = aws_subnet.public-subnet.*.id
}

output "aws_subnets_private" {
  value = aws_subnet.private-subnet.*.id
}

output "vpc_id" {
  value = aws_vpc.main-vpc.id
}

output "availability_zones" {
  value = data.aws_availability_zones.available.names
}
