resource "aws_vpc" "main-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = "true"
  enable_dns_support   = "true"

  tags = {
    Name = "main-vpc-${var.environment}"
  }
}

# Creaet three private Subnets
resource "aws_subnet" "public-subnet" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.main-vpc.id
  cidr_block              = "10.0.${0 + count.index}.0/24"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  # This function is neccessary in delete and create VPCs base on their
  # depandancies to other resources
  depends_on = [aws_vpc.main-vpc]

  tags = {
    Name = "fargate-public-subnet-${count.index + 1}-${var.environment}"
  }
}

# Creaet three private Subnets
resource "aws_subnet" "private-subnet" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.main-vpc.id
  cidr_block              = "10.0.${10 + count.index}.0/24"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false # to have a private IP add
  # This function is neccessary in delete and create VPCs base on their
  # depandancies to other resources
  depends_on = [aws_vpc.main-vpc]

  tags = {
    Name = "fargate-private-subnet-${count.index + 1}-${var.environment}"
  }
}


resource "aws_internet_gateway" "gw" {
  vpc_id     = aws_vpc.main-vpc.id
  depends_on = [aws_vpc.main-vpc]

  tags = {
    Name = "eks-internet-gateway-${var.environment}"
  }
}

resource "aws_eip" "nat" {
  vpc              = true
  count            = length(data.aws_availability_zones.available.names)
  public_ipv4_pool = "amazon"
}

resource "aws_nat_gateway" "gw" {
  count         = length(data.aws_availability_zones.available.names)
  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.public-subnet.*.id, count.index)
  # This function is neccessary in delete and create VPCs base on their
  # depandancies to other resources
  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = "eks-nat_Gateway-${count.index + 1}-${var.environment}"
  }
}

resource "aws_route_table" "internet-route" {
  vpc_id = aws_vpc.main-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  # This function is neccessary in delete and create VPCs base on their
  # depandancies to other resources
  depends_on = [aws_vpc.main-vpc]
  tags = {
    Name = "eks-public_route_table-${var.environment}"
  }
}

resource "aws_route_table" "nat-route" {
  vpc_id = aws_vpc.main-vpc.id
  count  = length(data.aws_availability_zones.available.names)
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = element(aws_nat_gateway.gw.*.id, count.index)
  }
  # This function is neccessary in delete and create VPCs base on their
  # depandancies to other resources
  depends_on = [aws_vpc.main-vpc]
  tags = {
    Name = "eks-nat_route_table-${count.index + 1}-${var.environment}"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.public-subnet.*.id, count.index)
  route_table_id = aws_route_table.internet-route.id

  depends_on = [aws_route_table.internet-route,
    aws_subnet.public-subnet
  ]
}


resource "aws_route_table_association" "private" {
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = element(aws_subnet.private-subnet.*.id, count.index)
  route_table_id = element(aws_route_table.nat-route.*.id, count.index)
  depends_on = [aws_route_table.nat-route,
    aws_subnet.private-subnet
  ]
}
