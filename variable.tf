variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {}
variable "environment" {
  description = "Environment name"
  default     = "k8s"
}

variable "cluster_name" {
  description = "Name of cluster"
  default     = "Kubernetes-cluster"
}

variable "fargate_namespace" {
  description = "Name of fargate selector namespace"
  default     = "k8s-fargate-app"
}
