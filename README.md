### Create Kubernetes cluster on AWS provider using terraform 

- To spin the K8s cluster and update .kube/config file.
`aws eks update-kubeconfig --name <cluster-name> --region <region>`
